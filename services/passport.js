var passport = require('passport');
var User = require('../model/user');
var config = require('../config');
var JwtStrategy = require('passport-jwt').Strategy;
var ExtractJwt = require('passport-jwt').ExtractJwt;
var LocalStrategy = require('passport-local');

module.exports = function(passport) {
    var jwtOptions = {
        jwtFromRequest: ExtractJwt.fromHeader('authorization'),
        secretOrKey: config.secret
    };

    var jwtLogin = new JwtStrategy(jwtOptions, function(payload, done) {
        User.findById(payload.sub, (err, user) => {
            if (err) { return done(err, false); }
            if (user) {
                done(null, user);
            } else {
                done(null, false);
            }
        });
    });

    var localOptions = { usernameField: 'email' };

    var localLogin = new LocalStrategy(localOptions, (email, password, done) => {
        User.findOne({email: email}, (err, user) => {
            if (err) { return done(err); }
            if (!user) { return done(null, false); }
            user.comparePassword(password, (err, isMatch) => {
                if (err) { return done(err); }
                if (!isMatch) { return done(null, false); }
                return done(null, user);
            });
        });
    });

    passport.use(jwtLogin);
    passport.use(localLogin);
};