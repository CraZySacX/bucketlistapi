var mongoose = require('mongoose');
var bcrypt = require('bcrypt');

// Grab the mongoose Schema.
var Schema = mongoose.Schema;

// Setup our user schema.
var userSchema = new Schema({
    email: {
        type: String,
        unique: true,
        lowercase: true
    },
    password: String
});

userSchema.pre('save', function(next) {
    var user = this;

    bcrypt.genSalt(10, (err, salt) => {
        if (err) { return next(err); }

        bcrypt.hash(user.password, salt, (err, hash) => {
            if (err) { return next(err); }
            user.password = hash;
            next();
        });
    });
});

userSchema.methods.comparePassword = function(incoming_pw, callback) {
    bcrypt.compare(incoming_pw, this.password, (err, isMatch) => {
        if (err) { return callback(err); }
        callback(null, isMatch);
    });
};

// Create the user model
var model = mongoose.model('User', userSchema);

module.exports = model;