var express = require('express');
var http = require('http');
var bodyparser = require('body-parser');
var mongoose = require('mongoose');

// Create the express application
var app = express();
var router = require('./router');

// Use native promises
mongoose.Promise = global.Promise;
mongoose.set('debug', true);

// Setup the db connection.
mongoose.connect('mongodb://localhost:27017/bucket', {
    useMongoClient: true,
    promiseLibrary: global.Promise
}).then(() => {
    console.log("Connection to mongodb successful!");
}, (err) => {
    console.log(err);
});

// Integrate body-parser
app.use(bodyparser.json({ type: '*/*' }));
router(app);

// Setup the http server.
var port = process.env.PORT || 3000;
var server = http.createServer(app);
server.listen(port);
console.log('Server listening on ' + port);