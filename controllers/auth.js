var User = require('../model/user');
var jwt = require('jwt-simple');
var config = require('../config');

function createUserToken(user) {
    var now = new Date().getTime();
    return jwt.encode({ sub: user.id, iat: now }, config.secret);
}

exports.signup = (req, res, next) => {
    var email = req.body.email;
    var password = req.body.password;

    if (!email || !password) {
        return res.status(418).send({error: 'You must provide email and password.'});
    }

    User.findOne({email: email}, '', (err, existingUser) => {
        if (err) { return next(err); }

        if (existingUser) {
            return res.status(418).json({'error': 'User already exists'});
        }

        var user = new User({
            email: email,
            password: password
        });

        user.save((err) => {
            if (err) { return next(err); }
            res.json({token: createUserToken(user)});
        });
    });
};

exports.signin = (req, res, next) => {
    res.send({ token: createUserToken(req.user) });
};