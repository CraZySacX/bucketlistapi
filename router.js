var Auth = require('./controllers/auth');
var User = require('./model/user');
var passport = require('passport');
var passportService = require('./services/passport')(passport);
var requireAuth = passport.authenticate('jwt', {session: false});
var requireSignin = passport.authenticate('local', {session: false });

module.exports = (app) => {
    app.get('/', requireAuth, (req, res) => {
        res.send('Hello, World!');
    });
    app.post('/signin', requireSignin, Auth.signin);
    app.post('/signup', Auth.signup);
};